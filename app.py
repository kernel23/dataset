# -*- coding: utf-8 -*-
"""
Created on Thu Jan 01 22:05:55 2020

@author: Zakaria FAHRAOUI
"""

import numpy as np
from flask import Flask, request, render_template
from keras.models import model_from_json
from keras.preprocessing import image

# if K.image_data_format() == 'channels_first':
#     input_shape = (3, 224, 224)
# else:
#     input_shape = (224, 224, 3)

with open('data/model_json.json', 'r') as json_file:
    loaded_model_json = json_file.read()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("./data/model_saved.h5")
app = Flask(__name__, template_folder='./templates')


# loading the model

@app.route('/')
def home():
    return render_template('index.html')


@app.route('/success', methods=['POST'])
def success():
    if request.method == 'POST':
        f = request.files['file']
        f.save('images/' + f.filename)
        image_path = 'images/' + f.filename
        img = image.load_img(image_path, target_size=(224, 224, 3))
        # TODO redimensionner image 224x224
        img = np.expand_dims(img, axis=0)
        result = loaded_model.predict_classes(img)
        print(result)
        return render_template("successimage.html", name=f.filename)


if __name__ == "__main__":
    app.run(debug=True)
